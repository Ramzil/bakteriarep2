using UnityEngine;
using UnityEngine.UI;

public class MenuManager : GenericSingletonClass<MenuManager>
{
    public GameObject menuPanel;
    public GameObject continueBn;
    public GameObject newGameBn;
    public GameObject controllerBn;
    public GameObject quitBn;
    public GameObject settingPanel;
    public GameObject ratingPanel;
    public GameObject tipsPanel;
    public GameObject resetPanel;
    public GameObject resetPanelYes;
    public GameObject resetPanelNo;
    public GameObject resetPanelStart;

    public GameObject survivalModBn;

    [SerializeField]
    private Image musicChanger;
    private bool isMusicOn = false;
    [SerializeField]
    private Sprite[] musicChangerSprite;
    [SerializeField]
    private AudioSource musicAudioSorce;

    [SerializeField]
    private GameObject ratingPanel_topLevel;
    [SerializeField]
    private GameObject ratingPanel_lastLevel;
    [SerializeField]
    private GameObject ratingPanel_newRecordText;

    [SerializeField]
    private bool firstStart = false;
    [SerializeField]
    private bool isGameOnPause = false;
    [SerializeField]
    private bool isBetweenLevel = true;

    private void Start()
    {
        ChangeMusicController();
        continueBn.GetComponent<Button>().onClick.AddListener(ResumeGame);
        newGameBn.GetComponent<Button>().onClick.AddListener(NewGameButton);
        controllerBn.GetComponent<Button>().onClick.AddListener(ChangeController);
        quitBn.GetComponent<Button>().onClick.AddListener(QuitGame);
        survivalModBn.GetComponent<Button>().onClick.AddListener(SurvivalModButton);
        PauseGame();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.gameStarted && isBetweenLevel && !SurvivalModManager.Instance.survivalModUI.activeSelf)
        {
            VfxManager.Instance.PlayClickButton();
            if (isGameOnPause)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }

        else if (Input.GetKeyDown(KeyCode.Escape) && SurvivalModManager.Instance.IsSurvivalModActive() && !GameManager.Instance.gameStarted && !SurvivalModManager.Instance.GetConditionSurvivalModNotufy())
        {
            SurvivalModManager.Instance.SurvivalModUCanceled();
        }

        else if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.gameStarted && !SurvivalModManager.Instance.survivalModUI.activeSelf)
        {
            UIManager.Instance.BossSpawnImReadyButton();
        }

        else if (Input.GetKeyDown(KeyCode.Escape) &&!SurvivalModManager.Instance.GetConditionSurvivalModNotufy())
        {
            VfxManager.Instance.PlayClickButton();
            CloseAllUI();
            SurvivalModManager.Instance.SurvivalModUCanceled();
        }
    }

    public bool GetConditionFirstStart()
    {
        return firstStart;
    }
    public void SetConditionFirstStart(bool newCond)
    {
        firstStart = newCond;
    }
    public void SetConditionBetwenLevel(bool newCond)
    {
        isBetweenLevel = newCond;
    }
    public void SurvivalModButton()
    {
        if (SurvivalModManager.Instance.IsSurvivalModActive())
        {
            SurvivalModManager.Instance.SurvivalModUIResetNotify();
        }
        else
        {
            SurvivalModManager.Instance.SurvivalModActivate();
        }

    }

    public bool IsGameOnPause()
    {
        return isGameOnPause;
    }

    public void PauseGame()
    {
        menuPanel.SetActive(true);

        if (firstStart)
            continueBn.SetActive(true);
        else
            continueBn.SetActive(false);

        Pause();
    }
    private void CloseAllUI()
    {
        CloseSettingPanel();
        CloseRatingPanel();
        CloseTipsPanel();
        CloseResetPanel();
    }
    public void Pause2()
    {
        isGameOnPause = true;
        Time.timeScale = 0f;
    }

    public void Pause()
    {
        isGameOnPause = true;
        Time.timeScale = 0f;
    }
    public void UnPause()
    {
        isGameOnPause = false;
        Time.timeScale = 1;
    }

    public void ResumeGame()
    {
        menuPanel.SetActive(false);
        CloseAllUI();
        UnPause();
    }

    public void NewGameButton()
    {
        OpenResetPanel();
    }

    public void StartNewGame()
    {
        SurvivalModManager.Instance.SetConditionSurvivalModActive(false);
        VfxManager.Instance.PlayClickButton();
        firstStart = true;
        GameManager.Instance.StartNewGame();
        ResumeGame();
    }

    private void QuitGame()
    {
        Application.Quit();
    }

    private void ChangeController()
    {
        VfxManager.Instance.PlayClickButton();
        Text controllerText = controllerBn.transform.Find("controllerText").gameObject.GetComponent<Text>();

        if (PlayerObject.Instance.MauseCondition())
        {
            controllerText.text = LanguageManager.Instance.controller1String;
            PlayerObject.Instance.SetMouseCondition(false);
        }
        else
        {
            controllerText.text = LanguageManager.Instance.controller2String;
            PlayerObject.Instance.SetMouseCondition(true);

        }
    }

    public void CloseSettingPanel()
    {
        VfxManager.Instance.PlayClickButton();
        settingPanel.SetActive(false);
    }
    public void OpenSettingPanel()
    {
        VfxManager.Instance.PlayClickButton();
        settingPanel.SetActive(true);
    }


    public void CloseRatingPanel()
    {
        VfxManager.Instance.PlayClickButton();
        ratingPanel.SetActive(false);
    }
    public void OpenRatingPanel(bool fromMenu)
    {
        VfxManager.Instance.PlayClickButton();
        if (fromMenu)
        {
            StatisticsManager.Instance.SetActualLastNumbersUI();
            ratingPanel.SetActive(true);
            ratingPanel_lastLevel.SetActive(true);
            ratingPanel_topLevel.SetActive(true);
            ratingPanel_newRecordText.SetActive(false);
        }
        else
        {
            ratingPanel.SetActive(true);

            if (StatisticsManager.Instance.CheckNewTopLevel())
            {
                VfxManager.Instance.PlayAlertNewRecord();
                ratingPanel_lastLevel.SetActive(false);
                ratingPanel_topLevel.SetActive(true);
                ratingPanel_newRecordText.SetActive(true);
            }
            else
            {
                ratingPanel_lastLevel.SetActive(true);
                ratingPanel_topLevel.SetActive(false);
                ratingPanel_newRecordText.SetActive(false);
            }
        }
        LanguageManager.Instance.OpenStats();
    }

    public void DeleteSaves()
    {
        PlayerPrefs.DeleteAll();
    }
    public void OpenTipsPanel()
    {
        VfxManager.Instance.PlayClickButton();
        tipsPanel.SetActive(true);
    }
    public void CloseTipsPanel()
    {
        VfxManager.Instance.PlayClickButton();
        tipsPanel.SetActive(false);
    }

    public void OpenResetPanel()
    {
        VfxManager.Instance.PlayClickButton();
        resetPanel.SetActive(true);
        LanguageManager.Instance.SetRightNotificationText();
        DifficultManager.Instance.SetHardMode(false);

        if (firstStart)
        {
            resetPanelStart.SetActive(false);
            resetPanelYes.SetActive(true);
            resetPanelNo.SetActive(true);
        }
        else
        {
            resetPanelYes.SetActive(false);
            resetPanelNo.SetActive(false);
            resetPanelStart.SetActive(true);
        }
    }

    public void CloseResetPanel()
    {
        VfxManager.Instance.PlayClickButton();
        resetPanel.SetActive(false);
    }

    public void ChangeMusicController()
    {
        if(isMusicOn)
        {
            isMusicOn = false;
            musicAudioSorce.Stop();
            musicChanger.sprite = musicChangerSprite[1];
        }
        else
        {
            isMusicOn = true;
            musicAudioSorce.Play();
            musicChanger.sprite = musicChangerSprite[0];
        }
    }
}