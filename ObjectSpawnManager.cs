using UnityEngine;

[System.Serializable]
public class Achivments
{
    public string nameOfBooster;
    public Sprite achivmentPics;
    public string whoCanTakeIt;
    public bool timerOn;
    public float howLongBooster;
    public bool oneTime;
    public int howMuch;
    public int howMuchForBoss;
}
public class ObjectSpawnManager : GenericSingletonClass<ObjectSpawnManager>
{
    private Sprite currentSprite;
    public Achivments[] achivment;
    private float longBooster;
    private bool oneTimeGloabal;
    private int bosterID;
    [SerializeField]
    private BoosterScript boosterUI;
    [SerializeField]
    private BoosterScriptOneTime boosterUIOneTime;
    [SerializeField]
    private int objectBeforeHide = 15;
    private AudioSource objectTake;

    public int GetIntObjectBeforeHide()
    {
        return objectBeforeHide;
    }

    public void ActiveStartGameBoosters()
    {
        BoosterTaked(2,true,0);
        BoosterTaked(3, true, 0);
    }

    void Start()
    {
        objectTake = GetComponent<AudioSource>();
    }

    public void NewGame()
    {
        boosterUI.howLongBooster = 0;
        boosterUIOneTime.howMuch = 0;
    }

    public void BoosterSpecificSpawn(int currentBooster, Vector3 pos, bool isItBossReward, int microbOrBoss)
    {
        var newKit = ObjectPooling.Instance.GetBoosterObject();
        newKit.gameObject.GetComponent<SpriteRenderer>().sprite = achivment[currentBooster].achivmentPics;
        newKit.boosterName = achivment[currentBooster].nameOfBooster;
        newKit.whoCanTakeItName = achivment[currentBooster].whoCanTakeIt;
        newKit.bossReward = isItBossReward;

        if (isItBossReward)
        {
            newKit.bossReward = true;
            newKit.howMuchBoosters = DifficultManager.Instance.GetBoosReward();
        }
        else
        {
            if (microbOrBoss == 1)
            {
                newKit.howMuchBoosters = Random.Range(1, 4);
            }
            else if (microbOrBoss == 2)
            {
                newKit.howMuchBoosters = achivment[currentBooster].howMuch;
            }
            else
            {
                Debug.LogError("some Error, microbOrBoss = " + microbOrBoss);
            }
        }

        newKit.boosterIndex = currentBooster;
        newKit.transform.position = pos;
        newKit.gameObject.SetActive(true);

        if (isItBossReward)
        {
            newKit.numberOfBooster.gameObject.transform.parent.gameObject.SetActive(true);
        }
        else
        {
            if (achivment[currentBooster].oneTime)
            {
                newKit.numberOfBooster.gameObject.transform.parent.gameObject.SetActive(true);
            }
            else
            {
                newKit.numberOfBooster.gameObject.transform.parent.gameObject.SetActive(false);
            }
        }
        newKit.StartBooster();
    }


    public void BoosterSpawn(int currentBooster)
    {
        objectTake.clip = VfxManager.Instance.GetSound("freeBoosterCreate");
        objectTake.Play();

        float yDirection = ScreenManager.Instance.ScreenTop() - (ScreenManager.Instance.ScreenTop() * 30 / 100);
        float xDirection = ScreenManager.Instance.ScreenRight() - (ScreenManager.Instance.ScreenRight() * 15 / 100);

        Vector2 newPosition = new Vector2(Random.Range(-xDirection, xDirection), Random.Range(-yDirection, yDirection));

        var newKit = ObjectPooling.Instance.GetBoosterObject();
        newKit.gameObject.GetComponent<SpriteRenderer>().sprite = achivment[currentBooster].achivmentPics;
        newKit.boosterName = achivment[currentBooster].nameOfBooster;
        newKit.whoCanTakeItName = achivment[currentBooster].whoCanTakeIt;
        newKit.howMuchBoosters = achivment[currentBooster].howMuch;
        newKit.boosterIndex = currentBooster;
        newKit.transform.position = newPosition;
        newKit.bossReward = false;
        newKit.gameObject.SetActive(true);

        if (achivment[currentBooster].oneTime)
        {
            newKit.numberOfBooster.gameObject.transform.parent.gameObject.SetActive(true);
        }
        else
        {
            newKit.numberOfBooster.gameObject.transform.parent.gameObject.SetActive(false);
        }
        newKit.StartBooster();
    }

    public void BoosterTaked(int currentBooster, bool isItBossReward2, int howMuchBoosters)
    {
        objectTake.clip = VfxManager.Instance.GetSound("boosterTake");
        objectTake.Play();

        switch (currentBooster)
        {
            case 0: //debug log
                break;
            case 1: //medKit booster
                if (isItBossReward2)
                {
                    PlayerManager.Instance.SetLivesLeft(DifficultManager.Instance.GetBoosReward());
                }
                else
                {
                    PlayerManager.Instance.SetLivesLeft(howMuchBoosters);
                }
                UIManager.Instance.UpdateLivesUi(PlayerManager.Instance.GetLivesLeft());
                StatisticsManager.Instance.SetLives(1);
                break;
            case 2: // double bullet booster
                PlayerObject.Instance.SetBulletsPerShotCondition(true);

                if (isItBossReward2)
                {
                    float howMuchForBoss1 = achivment[currentBooster].howLongBooster * DifficultManager.Instance.GetBoosReward();
                    PlayerManager.Instance.AddDoubleBulletTimer(howMuchForBoss1,false);
                }
                else
                    PlayerManager.Instance.AddDoubleBulletTimer(achivment[currentBooster].howLongBooster, false);

                PlayerManager.Instance.SetDoubleBullet(true);
                StatisticsManager.Instance.SetDoubleShots(1);
                boosterUI.gameObject.SetActive(true);
                boosterUI.currentPic = achivment[currentBooster].achivmentPics;
                boosterUI.howLongBooster = PlayerManager.Instance.GetDoubleBulletTimer();
                boosterUI.StartBoosterUI();
                break;
            case 3: //teleport booster
                if (isItBossReward2)
                {
                    PlayerManager.Instance.TeleportActive(achivment[currentBooster].howMuch * DifficultManager.Instance.GetBoosReward());
                }
                else
                    PlayerManager.Instance.TeleportActive(achivment[currentBooster].howMuch);

                PlayerManager.Instance.currentTeleport = boosterUIOneTime;
                StatisticsManager.Instance.SetTeleports(1);
                boosterUIOneTime.gameObject.SetActive(true);
                boosterUIOneTime.currentPic = achivment[currentBooster].achivmentPics;

                if (isItBossReward2)
                {
                    int howMuchForBoss2 = achivment[currentBooster].howMuch * DifficultManager.Instance.GetBoosReward();
                    boosterUIOneTime.howMuch += howMuchForBoss2;
                }
                else
                    boosterUIOneTime.howMuch += achivment[currentBooster].howMuch;
                boosterUIOneTime.StartBoosterUI();
                break;
            default:
                break;
        }
    }
}
