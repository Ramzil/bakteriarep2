using System;
using UnityEngine;
using UnityEngine.UI;

public class StatisticsManager : GenericSingletonClass<StatisticsManager>
{
    private void Start()
    {
        LoadSavedTopStats();
    }

    [SerializeField]
    private Text statsTopNumbersGo;
    [SerializeField]
    private Text statsLastNumbersGo;
    [SerializeField]
    private int[] lastNumbers;
    [SerializeField]
    private int[] topNumbers;

    private readonly string zeroString = "-" + Environment.NewLine + Environment.NewLine;


    public int GetActualTopPoints()
    {
        return topNumbers[0];
    }

    public int GetActualLastPoints()
    {
        return lastNumbers[0];
    }

    public void NewGame()
    {
        for (int i = 0; i < lastNumbers.Length; i++)
        {
            lastNumbers[i] = 0;
        }
    }

    public void SetPoints(int points)
    {
        lastNumbers[0] = points;
    }
    public void SetTopLevel(int level)
    {
        lastNumbers[1] = level;
    }
    public void SetBullets(int bullet)
    {
        lastNumbers[2] += bullet;
    }
    public void SetBacteria(int aster)
    {
        lastNumbers[3] += aster;
    }
    public void SetMicrob(int ufo)
    {
        lastNumbers[4] += ufo;
    }
    public void SetBoss(int boss)
    {
        lastNumbers[5] += boss;
    }
    public void SetTeleports(int teleport)
    {
        lastNumbers[6] += teleport;
    }
    public void SetLives(int lives)
    {
        lastNumbers[7] += lives;
    }
    public void SetDoubleShots(int doubleShots)
    {
        lastNumbers[8] += doubleShots;
    }


    public void SetActualLastNumbersUI()
    {
        for (int i = 1; i < lastNumbers.Length; i++)
        {
            if (lastNumbers[i] == 0)
            {
                if (i == 1)
                {
                    statsLastNumbersGo.text = zeroString;
                }
                else if (i > 1 && !(i == (lastNumbers.Length - 1)))
                {
                    statsLastNumbersGo.text += zeroString;
                }
                else if (i == (lastNumbers.Length - 1))
                {
                    statsLastNumbersGo.text += "-";
                }
            }
            else
            {
                if (i == 1)
                    statsLastNumbersGo.text = lastNumbers[i] + Environment.NewLine + Environment.NewLine;
                else if (i > 1 && !(i == (lastNumbers.Length - 1)))
                    statsLastNumbersGo.text += lastNumbers[i] + Environment.NewLine + Environment.NewLine;
                else if (i == (lastNumbers.Length - 1))
                    statsLastNumbersGo.text += lastNumbers[i];
            }
        }
        SetActualTopNumbersUI();
    }

    public void SetActualTopNumbersUI()
    {
        for (int i = 1; i < topNumbers.Length; i++)
        {
            if (topNumbers[i] == 0)
            {
                if (i == 1)
                {
                    statsTopNumbersGo.text = zeroString;
                }
                else if (i > 1 && !(i == (topNumbers.Length - 1)))
                {
                    statsTopNumbersGo.text += zeroString;
                }
                else if (i == (topNumbers.Length - 1))
                {
                    statsTopNumbersGo.text += "-";
                }
            }
            else
            {
                if (i == 1)
                    statsTopNumbersGo.text = topNumbers[i] + Environment.NewLine + Environment.NewLine;
                else if (i > 1 && !(i == (topNumbers.Length - 1)))
                    statsTopNumbersGo.text += topNumbers[i] + Environment.NewLine + Environment.NewLine;
                else if (i == (topNumbers.Length - 1))
                    statsTopNumbersGo.text += topNumbers[i];
            }
        }
    }


    public bool CheckNewTopLevel()
    {
        bool topIsUpdated = false;

        for (int i = 1; i < lastNumbers.Length; i++)
        {
            if (lastNumbers[i] == 0)
            {
                if (i == 1)
                {
                    statsLastNumbersGo.text = zeroString;
                }
                else if (i > 1 && !(i == (lastNumbers.Length - 1)))
                {
                    statsLastNumbersGo.text += zeroString;
                }
                else if (i == (lastNumbers.Length - 1))
                {
                    statsLastNumbersGo.text += "-";
                }
            }
            else
            {
                if (i == 1)
                    statsLastNumbersGo.text = lastNumbers[i] + Environment.NewLine + Environment.NewLine;
                else if (i > 1 && !(i == (lastNumbers.Length - 1)))
                    statsLastNumbersGo.text += lastNumbers[i] + Environment.NewLine + Environment.NewLine;
                else if (i == (lastNumbers.Length - 1))
                    statsLastNumbersGo.text += lastNumbers[i];
            }
        }

        if (lastNumbers[0] > topNumbers[0])
        {
            topIsUpdated = true;
            lastNumbers.CopyTo(topNumbers, 0);
            // topNumbers = lastNumbers;
            SetActualTopNumbersUI();
            SaveNewStats();
        }
        return topIsUpdated;
    }

    private void SaveNewStats()
    {
        PlayerPrefs.SetInt("topNumbers[0]", topNumbers[0]);
        PlayerPrefs.SetInt("topNumbers[1]", topNumbers[1]);
        PlayerPrefs.SetInt("topNumbers[2]", topNumbers[2]);
        PlayerPrefs.SetInt("topNumbers[3]", topNumbers[3]);
        PlayerPrefs.SetInt("topNumbers[4]", topNumbers[4]);
        PlayerPrefs.SetInt("topNumbers[5]", topNumbers[5]);
        PlayerPrefs.SetInt("topNumbers[6]", topNumbers[6]);
        PlayerPrefs.SetInt("topNumbers[7]", topNumbers[7]);
        PlayerPrefs.SetInt("topNumbers[8]", topNumbers[8]);
    }
    private void LoadSavedTopStats()
    {
        //Debug.Log("new tops loaded");
        topNumbers[0] = PlayerPrefs.GetInt("topNumbers[0]", topNumbers[0]);
        topNumbers[1] = PlayerPrefs.GetInt("topNumbers[1]", topNumbers[1]);
        topNumbers[2] = PlayerPrefs.GetInt("topNumbers[2]", topNumbers[2]);
        topNumbers[3] = PlayerPrefs.GetInt("topNumbers[3]", topNumbers[3]);
        topNumbers[4] = PlayerPrefs.GetInt("topNumbers[4]", topNumbers[4]);
        topNumbers[5] = PlayerPrefs.GetInt("topNumbers[5]", topNumbers[5]);
        topNumbers[6] = PlayerPrefs.GetInt("topNumbers[6]", topNumbers[6]);
        topNumbers[7] = PlayerPrefs.GetInt("topNumbers[7]", topNumbers[7]);
        topNumbers[8] = PlayerPrefs.GetInt("topNumbers[8]", topNumbers[8]);
    }
}