using UnityEngine;

public class BacteriaObject : MonoBehaviour
{
    private Rigidbody2D _rb;

    private SpriteRenderer currentBacteria;

    [HideInInspector]
    public float currentSize = 0.5f;
    private int currentNumber=1;

    private void Awake()
    {
        currentBacteria = GetComponent<SpriteRenderer>();
        _rb = GetComponent<Rigidbody2D>();
    }

    public void StartMove(Vector2 thrust, int testSpeed)
    {
        transform.localScale = Vector3.one * currentSize;
        float torque = Random.Range(-BacteriaSpawn.Instance.GetTorque(), BacteriaSpawn.Instance.GetTorque());
        _rb.AddForce(thrust * testSpeed);
        _rb.AddTorque(torque);
    }

    void Update()
    {
        if (transform.position.x > ScreenManager.Instance.ScreenRight())
        {
            transform.position = new Vector2(ScreenManager.Instance.ScreenLeft(), transform.position.y);
        }
        if (transform.position.x < ScreenManager.Instance.ScreenLeft())
        {
            transform.position = new Vector2(ScreenManager.Instance.ScreenRight(), transform.position.y);
        }
        if (transform.position.y > ScreenManager.Instance.ScreenTop())
        {
            transform.position = new Vector2(transform.position.x, ScreenManager.Instance.ScreenBottom());
        }
        if (transform.position.y < ScreenManager.Instance.ScreenBottom())
        {
            transform.position = new Vector2(transform.position.x, ScreenManager.Instance.ScreenTop());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        string collisionTag = collision.gameObject.tag;

        if (collisionTag == "PlayerBullet" || collisionTag == "MicrobBullet")
        {
            int bacteriaSpeed = Random.Range(BacteriaSpawn.Instance.GetMinSpeed(), BacteriaSpawn.Instance.GetMaxSpeed());
            VfxManager.Instance.PlayExplosionSound(0);

            if (currentSize == BacteriaSpawn.Instance.GetBacteriaSize(0)|| currentSize == BacteriaSpawn.Instance.GetBacteriaSize(1))
            {
                for (int i = 0; i < BacteriaSpawn.Instance.GetCellsNumber(); i++)
                {
                    float thrustFromSpawner = BacteriaSpawn.Instance.GetThrust();
                    if (!BacteriaSpawn.Instance.GetConditionOtherObject())
                    {
                        Vector2 thrust = new Vector2(Random.Range(-thrustFromSpawner, thrustFromSpawner), Random.Range(-thrustFromSpawner, thrustFromSpawner));
                        var newBacteria = ObjectPooling.Instance.GetBacteria(1);
                        newBacteria.transform.position = transform.position;
                        newBacteria.transform.rotation = transform.rotation;
                        newBacteria.gameObject.SetActive(true);

                        if (currentSize == BacteriaSpawn.Instance.GetBacteriaSize(0))
                        {
                            newBacteria.currentSize = BacteriaSpawn.Instance.GetBacteriaSize(1);
                        }
                        else if (currentSize == BacteriaSpawn.Instance.GetBacteriaSize(1))
                        {
                            newBacteria.currentSize = BacteriaSpawn.Instance.GetBacteriaSize(2);
                        }
                        else
                        {
                            newBacteria.currentSize = BacteriaSpawn.Instance.GetBacteriaSize(2);
                        }
                        newBacteria.StartMove(thrust, bacteriaSpeed);
                    }
                    else
                    {
                        if (currentNumber == 1 || currentNumber == 2)
                        {
                            Vector2 thrust = new Vector2(Random.Range(-thrustFromSpawner, thrustFromSpawner), Random.Range(-thrustFromSpawner, thrustFromSpawner));
                            var newBacteria = currentNumber == 1 ? ObjectPooling.Instance.GetBacteria(2) : ObjectPooling.Instance.GetBacteria(3);
                            newBacteria.transform.position = transform.position;
                            newBacteria.transform.rotation = transform.rotation;
                            newBacteria.gameObject.SetActive(true);
                            newBacteria.currentNumber = currentNumber == 1 ? 2 : 3;
                            newBacteria.currentSize = currentNumber == 1 ? BacteriaSpawn.Instance.GetBacteriaSize(1) : BacteriaSpawn.Instance.GetBacteriaSize(2);
                            newBacteria.StartMove(thrust, bacteriaSpeed);
                        }
                    }
                }
            }
        }

        if (collisionTag == "PlayerBullet")
        {
            int points = 0;
            if (!BacteriaSpawn.Instance.GetConditionOtherObject())
            {
                if (currentSize == BacteriaSpawn.Instance.GetBacteriaSize(0))
                    points = BacteriaSpawn.Instance.ReturnCostForBacteriaKill(0);
                else if (currentSize == BacteriaSpawn.Instance.GetBacteriaSize(1))
                    points = BacteriaSpawn.Instance.ReturnCostForBacteriaKill(1);
                else if (currentSize == BacteriaSpawn.Instance.GetBacteriaSize(2))
                    points = BacteriaSpawn.Instance.ReturnCostForBacteriaKill(2);
            }
            else
            {
                if (currentNumber == 1)
                    points = BacteriaSpawn.Instance.ReturnCostForBacteriaKill(0);
                else if (currentNumber == 2)
                    points = BacteriaSpawn.Instance.ReturnCostForBacteriaKill(1);
                else if (currentNumber == 3)
                    points = BacteriaSpawn.Instance.ReturnCostForBacteriaKill(2);
                else
                {
                    Debug.LogError($"currentNumber: {currentNumber}, collisionTag: + {collisionTag}");
                }
            }
            GameManager.Instance.AddPoints(points);
        }

        if (collisionTag == "PlayerBullet" || collisionTag == "Player" || collisionTag == "MicrobObject" || collisionTag == "MicrobBullet")
        {
            ReturnToPool();
            BacteriaSpawn.Instance.BacteriaDestroy(collision.gameObject.transform);

            if (collisionTag == "PlayerBullet" || collisionTag == "Player")
                StatisticsManager.Instance.SetBacteria(1);
        }
    }

    public void ReturnToPool()
    {
        if (!BacteriaSpawn.Instance.GetConditionOtherObject())
            ObjectPooling.Instance.ReturnBacteriaToPool(this,1);
        else
        {
            if (currentNumber == 1)
                ObjectPooling.Instance.ReturnBacteriaToPool(this,1);
            else if (currentNumber == 2)
                ObjectPooling.Instance.ReturnBacteriaToPool(this,2);
            else if (currentNumber == 3)
                ObjectPooling.Instance.ReturnBacteriaToPool(this,3);
            else
            {
                Debug.LogError("currentNumber: " + currentNumber);
            }
        }
    }
}
