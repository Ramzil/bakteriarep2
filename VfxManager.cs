using UnityEngine;

[System.Serializable]
public class AudioClipsArray
{
    public string nameOfClip;
    public AudioClip nameOfClips;
}

public class VfxManager : GenericSingletonClass<VfxManager>
{
    public AudioClipsArray[] totalClips;
    [SerializeField]
    private ParticleSystem explosion;
    private AudioSource explosionAudio;
    [SerializeField]
    private AudioSource musicAudioSorce;
    [SerializeField]
    private AudioClip[] explosionClips;
    [SerializeField]
    private AudioClip[] musicClips;

    private int currentMusicIndex = 99;

    public override void Awake()
    {
        explosionAudio = GetComponent<AudioSource>();
    }
    private void Start()
    {
        PlayMusic(0);
    }
    public AudioClip GetSound(string needclip)
    {
        AudioClip foundClip = null;
        for (int i = 0; i < totalClips.Length; i++)
        {
            if (totalClips[i].nameOfClip == needclip)
                foundClip = totalClips[i].nameOfClips;
        }
        //Debug.Log(foundClip == null ? "foundClip = NULL" : $"foundClip = {foundClip}");
        return foundClip;
    }
    public void PlayExplosionEffect(Vector3 position)
    {
        explosion.transform.position = position;
        explosion.Play();
    }
    public void PlayExplosionSound(int soundNumber)
    {
        explosionAudio.clip = explosionClips[soundNumber];
        explosionAudio.Play();
    }
    public void PlayClickButton()
    {
        explosionAudio.clip = GetSound("buttonClick");
        explosionAudio.Play();
    }
    public void PlayMusic(int currentSong)
    {
        currentMusicIndex = currentSong;
        musicAudioSorce.clip = musicClips[currentSong];
        musicAudioSorce.Play();
    }
    public void PlayAlertNewRecord()
    {
        explosionAudio.clip = GetSound("alertNewRecord");
        explosionAudio.Play();
    }
    public int GetCurrentMusicIndex()
    {
        return currentMusicIndex;
    }
}
