using UnityEngine;
using UnityEngine.UI;

public class TextureManager : GenericSingletonClass<TextureManager>
{
    [SerializeField]
    private SpriteRenderer bgGo;

    //ui textures
    [SerializeField]
    private Image mainMenuImgGo;
    [SerializeField]
    private Image settingImgGo;
    [SerializeField]
    private Image ratingsImgGo1;
    [SerializeField]
    private Image ratingsImgGo2;
    [SerializeField]
    private Image tipsImgGo;
    [SerializeField]
    private Image resetImgGo;
    [SerializeField]
    private Image resetImgGo2;

    [SerializeField]
    private Sprite[] bgTotalSprites;

    [SerializeField]
    private Sprite bulletPlayer;
    [SerializeField]
    private Sprite bulletMicrobe;

    //ui textures
    [SerializeField]
    private Sprite mainMenuTexture;
    [SerializeField]
    private Sprite settingTexture;
    [SerializeField]
    private Sprite ratingsTexture;
    [SerializeField]
    private Sprite tipsTexture;
    [SerializeField]
    private Sprite resetTexture;

    private void Start()
    {
        mainMenuImgGo.sprite = mainMenuTexture;
        settingImgGo.sprite = settingTexture;
        ratingsImgGo1.sprite = ratingsTexture;
        ratingsImgGo2.sprite = ratingsTexture;
        tipsImgGo.sprite = tipsTexture;
        resetImgGo.sprite = resetTexture;
        resetImgGo2.sprite = resetImgGo.sprite;
    }

    public void SetNewBg()
    {
        int currentLevel = GameManager.Instance.currentLevel.value;

        /*
        //change Bg each level
        switch (currentLevel)
        {
            case 1: 
                break;
        }
        */
    }
    public Sprite GetBulletPlayerSprite()
    {
        return bulletPlayer;
    }
    public Sprite GetBulletMicrobSprite()
    {
        return bulletMicrobe;
    }
}
