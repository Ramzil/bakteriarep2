using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : GenericSingletonClass<ObjectPooling>
{
    [SerializeField]
    private Transform bulletParent;
    [SerializeField]
    private Transform[] parentsForBakteriaGo;
    [SerializeField]
    private Transform otherObjectsParent;
    [SerializeField]
    private Transform boosterUIParent;
    [SerializeField]
    private BulletPrefab bulletPrefab;
    [SerializeField]
    private BacteriaObject[] bakteriaPrefabs;

    [SerializeField]
    private BoosterObject boostertObjectPrefab;
    [SerializeField]
    private BoosterScript boostertUIPrefab;

    private Queue<BulletPrefab> bullets = new Queue<BulletPrefab>();
    private Queue<BacteriaObject> bakteriaX3 = new Queue<BacteriaObject>();
    private Queue<BacteriaObject> bakteriaX2 = new Queue<BacteriaObject>();
    private Queue<BacteriaObject> bakteriaX1 = new Queue<BacteriaObject>();
    private Queue<BoosterObject> boosterObject = new Queue<BoosterObject>();
    private Queue<BoosterScript> boosterUi = new Queue<BoosterScript>();

    private int currentPrefab;
 

    #region bakteria pools
    public BacteriaObject GetBacteria(int whatPool)
    {
        switch (whatPool)
        {
            case 1:
                if (bakteriaX3.Count == 0)
                {
                    currentPrefab = 0;
                    AddBakteria(1, whatPool);
                }
                return bakteriaX3.Dequeue();
            case 2:
                if (bakteriaX2.Count == 0)
                {
                    currentPrefab = 1;
                    AddBakteria(1, whatPool);
                }
                return bakteriaX2.Dequeue();
            case 3:
                if (bakteriaX1.Count == 0)
                {
                    currentPrefab = 2;
                    AddBakteria(1, whatPool);
                }
                return bakteriaX1.Dequeue();
        }
        return null;
    }

    private void AddBakteria(int count, int currentPool)
    {
        BacteriaObject newBakteria = Instantiate(bakteriaPrefabs[currentPrefab]);
        newBakteria.transform.SetParent(parentsForBakteriaGo[currentPrefab], false);
        newBakteria.gameObject.SetActive(false);
        switch (currentPool)
        {
            case 1:
                bakteriaX3.Enqueue(newBakteria);
                break;
            case 2:
                bakteriaX2.Enqueue(newBakteria);
                break;
            case 3:
                bakteriaX1.Enqueue(newBakteria);
                break;
        }
    }

    public void ReturnBacteriaToPool(BacteriaObject newBakteria, int witchPool)
    {
        newBakteria.gameObject.SetActive(false);
        switch (witchPool)
        {
            case 1:
                bakteriaX3.Enqueue(newBakteria);
                break;
            case 2:
                bakteriaX2.Enqueue(newBakteria);
                break;
            case 3:
                bakteriaX1.Enqueue(newBakteria);
                break;
        }
    }
    #endregion

    #region Bullet Pool

    public BulletPrefab GetBullet()
    {
        if(bullets.Count == 0)
        {
            AddBullet(1);
        }
        return bullets.Dequeue();
    }

    private void AddBullet(int count)
    {
        BulletPrefab newBullet = Instantiate(bulletPrefab);
        newBullet.transform.SetParent(bulletParent, false);
        newBullet.gameObject.SetActive(false);
        bullets.Enqueue(newBullet);
    }
    public void ReturnBulletToPool(BulletPrefab newBullet)
    {
        newBullet.gameObject.SetActive(false);
        bullets.Enqueue(newBullet);
    }
    #endregion

    #region BoosterObject Pool


    public BoosterObject GetBoosterObject()
    {
        if (boosterObject.Count == 0)
        {
            AddBoosterObject(1);
        }
        return boosterObject.Dequeue();
    }
    private void AddBoosterObject(int count)
    {
        BoosterObject newBoosterObject = Instantiate(boostertObjectPrefab);
        newBoosterObject.transform.SetParent(otherObjectsParent, false);
        newBoosterObject.gameObject.SetActive(false);
        boosterObject.Enqueue(newBoosterObject);
    }
    public void ReturnBoosterObjectToPool(BoosterObject newBoosterObject)
    {
        newBoosterObject.gameObject.SetActive(false);
        boosterObject.Enqueue(newBoosterObject);
    }
    #endregion

    public void ReturnObjectToPools()
    {
        for (int i = 0; i < parentsForBakteriaGo.Length; i++)
        {
            foreach (Transform child in parentsForBakteriaGo[i])
                child.GetComponent<BacteriaObject>().ReturnToPool();
        }
        foreach (Transform child in bulletParent)
            child.GetComponent<BulletPrefab>().ReturnToPool();
        foreach (Transform child in otherObjectsParent)
            child.GetComponent<BoosterObject>().ReturnToPool();
        foreach (Transform child in boosterUIParent)
            child.gameObject.SetActive(false);
    }
    public Transform GetParentOfBakteria(int whatParentNeed)
    {
        return parentsForBakteriaGo[whatParentNeed];
    }
    public int GetParentArrayLength()
    {
        return parentsForBakteriaGo.Length;
    }
}
