using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : GenericSingletonClass<ScreenManager>
{
    private Camera cam;
    private float sceneWidth;
    private float sceneHeight;
    private float screenRight;
    private float screenLeft;
    private float screenTop;
    private float screenBottom;

    private void Start()
    {
        cam = GameObject.Find("Camera").GetComponent<Camera>();
        sceneWidth = cam.orthographicSize * 2 * cam.aspect;
        sceneHeight = cam.orthographicSize * 2;
        screenRight = sceneWidth / 2;
        screenLeft = screenRight * -1;
        screenTop = sceneHeight / 2;
        screenBottom = screenTop * -1;
    }
    public Camera MainCamera()
    {
        return cam;
    }
    public float ScreenRight()
    {
        return screenRight;
    }
    public float ScreenLeft()
    {
        return screenLeft;
    }
    public float ScreenTop()
    {
        return screenTop;
    }
    public float ScreenBottom()
    {
        return screenBottom;
    }
}
