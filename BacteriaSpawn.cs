using UnityEngine;

public class BacteriaSpawn : GenericSingletonClass<BacteriaSpawn>
{
    [SerializeField]
    private int[] costForBacteriaKill = new int[] { 20, 50, 100 };
    [SerializeField]
    private int timeOutLevels;
    [SerializeField]
    private int timeOutSpawn;
    [SerializeField]
    private int spawnDistance;

    private int bacteriaSpawnStartLevel;
    [SerializeField]
    private float varianceRange;
    [SerializeField]
    private int defaultSpawn = 1;
    [SerializeField]
    private int howMuchCells;
    [SerializeField]
    private float testThrust;
    [SerializeField]
    private float testTorque;
    [SerializeField]
    private int bacteriaSpeed;
    [SerializeField]
    private int bacteriaMinSpeed;
    [SerializeField]
    private int bacteriaMaxSpeed;
    [SerializeField]
    private bool otherObject = false;
    [SerializeField]
    private float[] bacteriaSize;

    public int ReturnCostForBacteriaKill(int whatIndex)
    {
       return costForBacteriaKill[whatIndex];
    }
    public bool GetConditionOtherObject()
    {
        return otherObject;
    }
    public int GetMinSpeed()
    {
        return bacteriaMinSpeed;
    }
    public int GetMaxSpeed()
    {
        return bacteriaMaxSpeed;
    }

    public float GetTorque()
    {
        return testTorque;
    }
    public float GetThrust()
    {
        return testThrust;
    }
    public float GetBacteriaSize(int Index)
    {
        return bacteriaSize[Index];
    } 
    public int GetCellsNumber()
    {
        return howMuchCells;
    }

    public void SetStartLevel(int incomInt)
    {
        bacteriaSpawnStartLevel = incomInt;
    }

    public void NewGame()
    {
        bacteriaSpawnStartLevel = defaultSpawn;
    }

    public int GetDefaultSpawn()
    {
        return defaultSpawn;
    }
    public void StartNewLevel()
    {
        bacteriaSpawnStartLevel++;
        StartSpawn();
    }

    public void StartSpawn()
    {
        Invoke(nameof(SpawnBacteria), timeOutLevels);
    }

    private void SpawnBacteria()
    {
        for (int i = 0; i < bacteriaSpawnStartLevel; i++)
        {
            Vector3 spawnDirection = Random.insideUnitCircle.normalized * spawnDistance;
            Vector3 spawnPoint = transform.position + spawnDirection;
            float variance = Random.Range(-varianceRange, varianceRange);
            Quaternion rotation = Quaternion.AngleAxis(variance, Vector3.forward);
            var newBacteria = ObjectPooling.Instance.GetBacteria(1);
            newBacteria.transform.position = spawnPoint;
            newBacteria.transform.rotation = rotation;
            newBacteria.gameObject.SetActive(true);
            newBacteria.currentSize = bacteriaSize[0];
            Vector2 thrust = new Vector2(Random.Range(-testThrust, testThrust), Random.Range(-testThrust, testThrust));
            bacteriaSpeed = Random.Range(bacteriaMinSpeed, bacteriaMaxSpeed);
            newBacteria.StartMove(thrust, bacteriaSpeed);
        }
        CancelInvoke("SpawnBacteria");
    }

    public void BacteriaDestroy(Transform bacteriaPosition)
    {
        VfxManager.Instance.PlayExplosionEffect(bacteriaPosition.position);
        GameManager.Instance.CheckBacteriaInScene();
    }

    private void CheckInvoke()
    {
        GameManager.Instance.CheckBacteriaInScene();
    }
}
