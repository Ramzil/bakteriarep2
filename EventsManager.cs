using UnityEngine;

public class EventsManager : GenericSingletonClass<EventsManager>
{
    [SerializeField]
    private float boosterDefault;
    [SerializeField]
    private float boosterSpawnTimer;
    [SerializeField]
    private float minBoosterSpawnTimer;
    [SerializeField]
    private float maxBoosterSpawnTimer;

    private void Update()
    {
        boosterSpawnTimer -= Time.deltaTime;
        if (boosterSpawnTimer <= 0)
        {
            int random = Random.Range(2, ObjectSpawnManager.Instance.achivment.Length);
            ObjectSpawnManager.Instance.BoosterSpawn(random);
            boosterSpawnTimer = Random.Range(minBoosterSpawnTimer, maxBoosterSpawnTimer);
        }
    }

    public void NewGame()
    {
        boosterSpawnTimer = boosterDefault;
    }
    public void SetMaxBoosterSpawnTimer(float newTimer)
    {
        maxBoosterSpawnTimer = newTimer;
    }
    public void SetMinBoosterSpawnTimer(float newTimer)
    {
        minBoosterSpawnTimer = newTimer;
    }
}
