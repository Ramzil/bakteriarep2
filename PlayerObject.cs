using UnityEngine;

public class PlayerObject : GenericSingletonClass<PlayerObject>
{
    private Rigidbody2D _rb;
    [SerializeField]
    private float thrust = 6f;
    [SerializeField]
    private float turnSpeed = 5f;
    [SerializeField]
    private float maxSpeed = 4.5f;
    [SerializeField]
    private float bulletSpeed;
    [SerializeField]
    private float bulletSize = 0.8f;
    [SerializeField]
    private Color bulletColor = Color.white;
    [SerializeField]
    private float refireRate = 2f;
    [SerializeField]
    private float teleportDelay;
    [SerializeField]
    private Transform bulletTransform; 
    private float fireTimer = 2f;
    private bool teleport = false;
    private bool mouseController = false;
    private AudioSource playerSound;
    [SerializeField]
    private float timeOutToHideBullet = 1f;
    [SerializeField]
    private bool bulletsPerShot = false;
    [SerializeField]
    private bool shotDone = true;
    [SerializeField]
    private float timeOutBetwenFire = 1;

    public float GetTimeToHideBullet()
    {
        return timeOutToHideBullet;
    }
    public bool MauseCondition()
    {
        return mouseController;
    }
    public void SetMouseCondition(bool newCond)
    {
        mouseController = newCond;
    }
    public void SetBulletsPerShotCondition(bool newCond)
    {
        bulletsPerShot = newCond;
    }

    void Start()
    {
        playerSound = GetComponent<AudioSource>();
        playerSound.clip = VfxManager.Instance.GetSound("playerShot");
        _rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        fireTimer += Time.deltaTime;

        if (Input.GetButtonDown("Fire1") && !MenuManager.Instance.IsGameOnPause())
        {
            if (fireTimer >= refireRate)
            {
                fireTimer = 0;
                shotDone = true;
                Fire();
            }
        }

        if (mouseController)
        {
            Vector3 direction = ScreenManager.Instance.MainCamera().ScreenToWorldPoint(Input.mousePosition) - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, turnSpeed * Time.deltaTime);
        }
        
        if (Input.GetButtonDown("Teleport") && !teleport&& PlayerManager.Instance.GetTeleportCondition())
        {
            PlayerManager.Instance.UpdateTeleport();
            playerSound.clip = VfxManager.Instance.GetSound("playerTeleport");
            playerSound.Play();
            teleport = true;
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<CapsuleCollider2D>().enabled = false;
            Invoke("Teleport", teleportDelay);
        }
    }

    private void Teleport()
    {
        float yDirection = ScreenManager.Instance.ScreenTop() - (ScreenManager.Instance.ScreenTop() * 30 / 100);
        float xDirection = ScreenManager.Instance.ScreenRight() - (ScreenManager.Instance.ScreenRight() * 15 / 100);

        Vector2 teleportPosition = new Vector2(Random.Range(-xDirection, xDirection), Random.Range(-yDirection, yDirection));
        transform.position = teleportPosition;
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<CapsuleCollider2D>().enabled = true;
        teleport = false;
    }

    void FixedUpdate()
    {
        if (Input.GetAxis("Vertical") > 0)
        {
            _rb.AddForce(transform.right * thrust * Input.GetAxis("Vertical"));
            _rb.velocity = new Vector2(Mathf.Clamp(_rb.velocity.x, -maxSpeed, maxSpeed), Mathf.Clamp(_rb.velocity.y, -maxSpeed, maxSpeed));
        }
        if (!mouseController)
        {
            float rotationSpeed = turnSpeed * 50;
            transform.Rotate(0, 0, Input.GetAxis("Horizontal") * -rotationSpeed * Time.deltaTime);
        }
        if (transform.position.x > ScreenManager.Instance.ScreenRight())
        {
            transform.position = new Vector2(ScreenManager.Instance.ScreenLeft(), transform.position.y);
        }
        if (transform.position.x < ScreenManager.Instance.ScreenLeft())
        {
            transform.position = new Vector2(ScreenManager.Instance.ScreenRight(), transform.position.y);
        }
        if (transform.position.y > ScreenManager.Instance.ScreenTop())
        {
            transform.position = new Vector2(transform.position.x, ScreenManager.Instance.ScreenBottom());
        }
        if (transform.position.y < ScreenManager.Instance.ScreenBottom())
        {
            transform.position = new Vector2(transform.position.x, ScreenManager.Instance.ScreenTop());
        }
    }

    public void ResetPlayer()
    {
        transform.position = new Vector2(0f, 0f);
        transform.eulerAngles = new Vector3(0, 180f, 0);
        _rb.velocity = new Vector3(0f, 0f, 0f);
    }

    private void Fire()
    {
        PlayerManager.Instance.PlayShotAnimation();
        playerSound.clip = VfxManager.Instance.GetSound("playerShot");
        playerSound.Play();
        StatisticsManager.Instance.SetBullets(1);

        var newBullet = ObjectPooling.Instance.GetBullet();
        newBullet.GetComponent<SpriteRenderer>().sprite = TextureManager.Instance.GetBulletPlayerSprite();
        newBullet.gameObject.tag = "PlayerBullet";
        newBullet.GetComponent<SpriteRenderer>().color = bulletColor;
        newBullet.transform.position = bulletTransform.position;
        newBullet.transform.rotation = bulletTransform.rotation;
        newBullet.gameObject.SetActive(true);
        newBullet.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * bulletSpeed);
        newBullet.transform.localScale = Vector3.one * bulletSize;

        if (bulletsPerShot && shotDone)
        {
            shotDone = false;
            Invoke(nameof(Fire), timeOutBetwenFire);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bacteria" || collision.gameObject.tag == "MicrobBullet" || collision.gameObject.tag == "MicrobObject" || collision.gameObject.tag == "VirusObject")
        {
            _rb.velocity = Vector3.zero;
            _rb.angularVelocity = 0.0f;
            PlayerManager.Instance.PlayerDead();
        }
    }
}