using UnityEngine;

public class BossVirusObject : MonoBehaviour
{
    private Transform player;
    [SerializeField]
    private float bulletForce;

    [SerializeField]
    private float fireTimer = 2f;
    [SerializeField]
    private float refireRate = 2f;
    [SerializeField]
    private int bossHealthPoints;

    private Rigidbody2D _rb;

    private void OnEnable()
    {
        bossHealthPoints = (BossManager.Instance.GetBossHealth() + BossManager.Instance.GetHowMuchBossKilled());
        BossManager.Instance.UpdateBossHPUI(bossHealthPoints);
    }
    private AudioSource bossSound;

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Transform>();
        bossSound = GetComponent<AudioSource>();
        bossSound.clip = VfxManager.Instance.GetSound("bossShot");
        _rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        fireTimer += Time.deltaTime;

        if (fireTimer >= refireRate)
        {
            fireTimer = 0;
            refireRate = Random.Range(2, 6);

            bossSound.clip = VfxManager.Instance.GetSound("bossShot");
            bossSound.Play();

            var newBacter = ObjectPooling.Instance.GetBacteria(1);
            newBacter.transform.position = transform.position;
            newBacter.transform.rotation = transform.rotation;
            newBacter.gameObject.SetActive(true);
            newBacter.transform.localScale = Vector3.one * BacteriaSpawn.Instance.GetBacteriaSize(0);
            newBacter.currentSize = BacteriaSpawn.Instance.GetBacteriaSize(0);

            Vector3 aim = (player.position - transform.position).normalized;
            float torque = Random.Range(-BacteriaSpawn.Instance.GetTorque(), BacteriaSpawn.Instance.GetTorque());

            newBacter.GetComponent<Rigidbody2D>().AddForce(aim * bulletForce);
            newBacter.GetComponent<Rigidbody2D>().AddTorque(torque);
        }

        if (transform.position.x > ScreenManager.Instance.ScreenRight())
        {
            transform.position = new Vector2(ScreenManager.Instance.ScreenLeft(), transform.position.y);
        }
        if (transform.position.x < ScreenManager.Instance.ScreenLeft())
        {
            transform.position = new Vector2(ScreenManager.Instance.ScreenRight(), transform.position.y);
        }
        if (transform.position.y > ScreenManager.Instance.ScreenTop())
        {
            transform.position = new Vector2(transform.position.x, ScreenManager.Instance.ScreenBottom());
        }
        if (transform.position.y < ScreenManager.Instance.ScreenBottom())
        {
            transform.position = new Vector2(transform.position.x, ScreenManager.Instance.ScreenTop());
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "PlayerBullet")
        {
            bossHealthPoints--;
            BossManager.Instance.UpdateBossHPUI(bossHealthPoints);
            bossSound.clip = VfxManager.Instance.GetSound("bossHurt");
            bossSound.Play();

            if (bossHealthPoints == 0)
            {
                _rb.velocity = Vector3.zero;
                _rb.angularVelocity = 0.0f;
                BossManager.Instance.BossDead();
                StatisticsManager.Instance.SetBoss(1);

                if (collision.gameObject.tag == "PlayerBullet" || collision.gameObject.tag == "Player")
                {
                    GameManager.Instance.AddPoints(BossManager.Instance.GetRewardBossKill());
                }
            }
        }
    }
    public void ContinueAfterAnimation() // call from animation
    {
        BossManager.Instance.StartAnimationBossDead();
    }
}

