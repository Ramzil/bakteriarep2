using UnityEngine;

public class MicrobObject : MonoBehaviour
{
    private Transform player;
    [SerializeField]
    private float bulletForce;

    [SerializeField]
    private float fireTimer = 2f;
    [SerializeField]
    private float refireRate = 2f;
    [SerializeField]
    private float defaultTimerMicrobHide = 10;
    [SerializeField]
    private float bulletSize = 1;
    [SerializeField]
    private Color bulletColor = new Color(110,165,0,255);
    private float currentTimeOutBeforMicrobHide;

    private Rigidbody2D _rb;
    private AudioSource microbSound;

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Transform>();
        microbSound = GetComponent<AudioSource>();
        microbSound.clip = VfxManager.Instance.GetSound("microbShot");
        _rb = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        currentTimeOutBeforMicrobHide = defaultTimerMicrobHide;
    }

    void Update()
    {
        fireTimer += Time.deltaTime;
        currentTimeOutBeforMicrobHide -= Time.deltaTime;

        if (fireTimer >= refireRate)
        {
            fireTimer = 0;
            refireRate = Random.Range(2, 6);

            microbSound.Play();
            var newBullet = ObjectPooling.Instance.GetBullet();
            newBullet.GetComponent<SpriteRenderer>().sprite = TextureManager.Instance.GetBulletMicrobSprite();
            newBullet.gameObject.tag = "MicrobBullet";
            newBullet.transform.position = transform.position;
            newBullet.transform.rotation = transform.rotation;
            newBullet.gameObject.SetActive(true);
            Vector3 aim = (player.position - transform.position).normalized;
            newBullet.GetComponent<Rigidbody2D>().AddForce(aim * bulletForce);
            newBullet.transform.localScale = Vector3.one * bulletSize;
            newBullet.GetComponent<SpriteRenderer>().color = bulletColor;
        }

        if (transform.position.x > ScreenManager.Instance.ScreenRight())
        {
            if (currentTimeOutBeforMicrobHide <= 0)
            {
                HideMicrob();
            }
            else
                transform.position = new Vector2(ScreenManager.Instance.ScreenLeft(), transform.position.y);
        }
        if (transform.position.x < ScreenManager.Instance.ScreenLeft())
        {
            if (currentTimeOutBeforMicrobHide <= 0)
            {
                HideMicrob();
            }
            else
                transform.position = new Vector2(ScreenManager.Instance.ScreenRight(), transform.position.y);
        }
        if (transform.position.y > ScreenManager.Instance.ScreenTop())
        {
            if (currentTimeOutBeforMicrobHide <= 0)
            {
                HideMicrob();
            }
            else
                transform.position = new Vector2(transform.position.x, ScreenManager.Instance.ScreenBottom());
        }
        if (transform.position.y < ScreenManager.Instance.ScreenBottom())
        {
            if (currentTimeOutBeforMicrobHide <= 0)
            {
                HideMicrob();
            }
            else
                transform.position = new Vector2(transform.position.x, ScreenManager.Instance.ScreenTop());
        }
    }

    private void HideMicrob()
    {
        currentTimeOutBeforMicrobHide = defaultTimerMicrobHide;
        MicrobManager.Instance.MicrobHide();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bacteria" || collision.gameObject.tag == "Player" || collision.gameObject.tag == "PlayerBullet")
        {
            _rb.velocity = Vector3.zero;
            _rb.angularVelocity = 0.0f;
            MicrobManager.Instance.MicrobDead();

            if (collision.gameObject.tag == "PlayerBullet" || collision.gameObject.tag == "Player")
            {
                StatisticsManager.Instance.SetMicrob(1);
                GameManager.Instance.AddPoints(MicrobManager.Instance.GetIntRewardForMicrobKill());
            }
        }
    }
}
