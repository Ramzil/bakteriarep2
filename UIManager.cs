using UnityEngine;
using UnityEngine.UI;

public class UIManager : GenericSingletonClass<UIManager>
{
    [SerializeField]
    public Text livesUIText;
    [SerializeField]
    public Text pointsEarnedUIText;
    [SerializeField]
    public Text levelsUIText;
    [SerializeField]
    public Text gameOverUIText;
    [SerializeField]
    public Text levelsUIText2;
    [SerializeField]
    public Text bossKilleUI;
    [SerializeField]
    public Text bossAdviceUI;
    [SerializeField]
    public Text bossStartTimerUI;
    [SerializeField]
    public GameObject gameOverUIGO;
    [SerializeField]
    public GameObject bossLevelButtonUIGO;
    [SerializeField]
    public GameObject bossLevelPausePanelUIGO;
    [SerializeField]
    public GameObject dieHardButton;
    [SerializeField]
    public GameObject pussyModeButton;

    public void SetFonts()
    {
        livesUIText.font = LanguageManager.Instance.firstFont;
        pointsEarnedUIText.font = LanguageManager.Instance.firstFont;
        levelsUIText.font = LanguageManager.Instance.firstFont;
        gameOverUIText.font = LanguageManager.Instance.firstFont;
        levelsUIText2.font = LanguageManager.Instance.firstFont;
        bossKilleUI.font = LanguageManager.Instance.firstFont;
        bossAdviceUI.font = LanguageManager.Instance.firstFont;
        bossStartTimerUI.font = LanguageManager.Instance.firstFont;
    }
    public void StartUI()
    {
        levelsUIText.gameObject.SetActive(false);
        gameOverUIGO.SetActive(false);
        UpdatePointsUi();
        UpdateLevelUi();
        SetFonts();
    }
    public void ActiveteLevelTextUI(string currentMessage, bool bossLevel, bool firstLevel)
    {
        GameManager.Instance.SetConditionBossLevel(bossLevel);
        levelsUIText.gameObject.SetActive(true);

        if (bossLevel)
        {
            MenuManager.Instance.SetConditionBetwenLevel(false);
            levelsUIText.text = LanguageManager.Instance.GetBossCurrentName(BossManager.Instance.GetHowMuchBossKilled()) + currentMessage;
            bossAdviceUI.text = LanguageManager.Instance.BossAdvice();
            bossLevelButtonUIGO.SetActive(true);
            bossLevelPausePanelUIGO.SetActive(true);
        }
        else
        {
            levelsUIText.text = currentMessage;

            bossLevelButtonUIGO.SetActive(true);

            if (firstLevel)
            {
                MenuManager.Instance.SetConditionBetwenLevel(false);
                bossLevelPausePanelUIGO.SetActive(true);
            }
        }
        MenuManager.Instance.Pause();
    }
    public void UpdatePointsUi()
    {
        pointsEarnedUIText.text = LanguageManager.Instance.scoreString + GameManager.Instance.GetPointNumber();
    }
    public void UpdateLevelUi()
    {
        levelsUIText2.text = LanguageManager.Instance.levelString + GameManager.Instance.GetCurrentLevelNumber();
    }

    public void UpdateLivesUi(int livesLeft)
    {
        livesUIText.text = LanguageManager.Instance.livesString + livesLeft;
    }
    public void ReUpdateLivesUi()
    {
        livesUIText.text = LanguageManager.Instance.livesString + PlayerManager.Instance.GetLivesLeft();
    }
    public void UpdateBossKilledUi()
    {
        bossKilleUI.text = LanguageManager.Instance.bossKilledString + BossManager.Instance.GetHowMuchBossKilled();
    }

    private void CheckForAds()
    {
        bossLevelButtonUIGO.SetActive(false);
        bossLevelPausePanelUIGO.SetActive(false);
        Invoke(nameof(DeActiveteLevelTextUI), 3f);
    }

    public void BossSpawnImReadyButton()
    {
        if (GameManager.Instance.IsItBossLevel())
        {
            MicrobManager.Instance.SetConditionMicrobSpawnTimer(false);
            GameManager.Instance.SetConditionBossTimer(true);
            bossStartTimerUI.gameObject.SetActive(true);
            bossAdviceUI.text = LanguageManager.Instance.CancelBossAdvice();
        }
        else if (!GameManager.Instance.IsItBossLevel() && SurvivalModManager.Instance.IsSurvivalModActive() && GameManager.Instance.GetCurrentLevelNumber() == 7)
        {
            VfxManager.Instance.PlayMusic(2);
        }
        levelsUIText.gameObject.SetActive(false);
        bossLevelPausePanelUIGO.gameObject.SetActive(false);
        MenuManager.Instance.UnPause();
        MenuManager.Instance.SetConditionBetwenLevel(true);

    }
    public void StartBossLevel()
    {
        bossStartTimerUI.gameObject.SetActive(false);
        BossManager.Instance.BossSpawn();
        MicrobManager.Instance.SetConditionMicrobSpawnTimer(true);
    }

    public void DeActiveteLevelTextUI()
    {
        levelsUIText.gameObject.SetActive(false);
    }
    private void InvokeGameMenu()
    {
        gameOverUIGO.SetActive(false);
        MenuManager.Instance.PauseGame();
        OpenRating();
    }
    private void OpenRating()
    {
        MenuManager.Instance.OpenRatingPanel(false);
    }
}
