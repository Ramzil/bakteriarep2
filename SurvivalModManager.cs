using UnityEngine;

public class SurvivalModManager : GenericSingletonClass<SurvivalModManager>
{
    public GameObject survivalModUI;
    public GameObject survivalModPromoteUI;
    [SerializeField]
    private GameObject survivalModReadyButton;
    [SerializeField]
    private GameObject survivalModRestNotify;

    [SerializeField]
    private bool survivalModActive = false;
    [SerializeField]
    private bool survivalModNotify = false;

    public bool GetConditionSurvivalModNotufy()
    {
        return survivalModNotify;
    }
    public bool IsSurvivalModActive()
    {
        return survivalModActive;
    }
    public void SetConditionSurvivalModActive(bool newCond)
    {
        survivalModActive = newCond;
    }

    public void SurvivalModActivate()
    {
        survivalModPromoteUI.SetActive(false);
        survivalModRestNotify.SetActive(false);
        survivalModActive = true;
        survivalModReadyButton.SetActive(true);
        VfxManager.Instance.PlayClickButton();
        survivalModUI.SetActive(true);
        MenuManager.Instance.menuPanel.SetActive(false);
        MenuManager.Instance.Pause();
        DifficultManager.Instance.SetHardMode(false);
    }
    public void SurvivalModUIResetNotify()
    {
        GameManager.Instance.gameStarted = false;
        survivalModNotify = true;
        survivalModRestNotify.SetActive(true);
        VfxManager.Instance.PlayClickButton();
        MenuManager.Instance.menuPanel.SetActive(false);
        MenuManager.Instance.Pause();
    }
    public void SurvivalModUIResumeGameNotify()
    {
        GameManager.Instance.gameStarted = true;
        survivalModNotify = false;
        survivalModRestNotify.SetActive(false);
        VfxManager.Instance.PlayClickButton();
        MenuManager.Instance.ResumeGame();
    }

    public void SurvivalModUIDeactive()
    {
        survivalModNotify = false;
        survivalModRestNotify.SetActive(false);
        MenuManager.Instance.SetConditionFirstStart(true);
        survivalModReadyButton.SetActive(false);
        VfxManager.Instance.PlayClickButton();
        survivalModUI.SetActive(false);
        GameManager.Instance.StartNewGame();
        MenuManager.Instance.ResumeGame();
    }
    public void SurvivalModUCanceled()
    {
        survivalModNotify = false;
        survivalModRestNotify.SetActive(false);
        survivalModActive = false;
        survivalModReadyButton.SetActive(false);
        VfxManager.Instance.PlayClickButton();
        survivalModUI.SetActive(false);
        MenuManager.Instance.menuPanel.SetActive(true);
    }
}
