using UnityEngine;
using UnityEngine.UI;

public class GameManager : GenericSingletonClass<GameManager>
{
    public Observable<int> points = new Observable<int>(0);
    public Observable<int> currentLevel = new Observable<int>(1);

    #region variables
    [SerializeField]
    private int gameOverTextTimeOut;
    [HideInInspector]
    public bool gameStarted = false;
    [SerializeField]
    private string gameVersion;
    [SerializeField]
    private Text versionText;
    private bool startBossTimer = false;
    private bool isItBossLevel = false;
    [SerializeField]
    private float startTimerDefaultFloat = 3;
    private float startTimerFloat = 3;
    private AudioSource levelResultSound;
    #endregion

    void OnEnable()
    {
        points.valueChanged += UIManager.Instance.UpdatePointsUi;
        currentLevel.valueChanged += UIManager.Instance.UpdateLevelUi;
    }

    void Start()
    {
        startTimerFloat = startTimerDefaultFloat;
        levelResultSound = GetComponent<AudioSource>();

        versionText.text = "v." + gameVersion;
        points.value = 0;
        UIManager.Instance.StartUI();
    }

    private void Update()
    {
        if (startBossTimer)
        {
            startTimerFloat -= Time.deltaTime;
            UIManager.Instance.bossStartTimerUI.text = ""+(int)startTimerFloat;
            if (startTimerFloat < 0)
            {
                startBossTimer = false;
                startTimerFloat = startTimerDefaultFloat;
                UIManager.Instance.StartBossLevel();
            }
        }
    }
    #region incapsulation

    public int GetPointNumber()
    {
        return points.value;
    }
    public int GetCurrentLevelNumber()
    {
        return currentLevel.value;
    }
    public bool IsItBossLevel()
    {
        return isItBossLevel;
    }
    public void SetConditionBossLevel(bool newCond)
    {
        isItBossLevel = newCond;
    }
    public void SetConditionBossTimer(bool newCond)
    {
        startBossTimer = newCond;
    }
    #endregion

    public void StartNewGame()
    {
        MenuManager.Instance.SetConditionBetwenLevel(true);
        CancelInvoke();
        if (VfxManager.Instance.GetCurrentMusicIndex() != 0)
            VfxManager.Instance.PlayMusic(0);

        startBossTimer = false;
        startTimerFloat = startTimerDefaultFloat;
        UIManager.Instance.bossStartTimerUI.gameObject.SetActive(false);
        gameStarted = true;
        points.value = 0;
        currentLevel.value = 1;
        BossManager.Instance.NewGame();
        TextureManager.Instance.SetNewBg();
        StatisticsManager.Instance.SetTopLevel(currentLevel.value);
        BacteriaSpawn.Instance.NewGame();
        PlayerManager.Instance.NewGame();
        MicrobManager.Instance.NewGame();
        EventsManager.Instance.NewGame();
        StatisticsManager.Instance.NewGame();
        ObjectSpawnManager.Instance.NewGame();

        UIManager.Instance.bossKilleUI.text = LanguageManager.Instance.bossKilledString + BossManager.Instance.GetHowMuchBossKilled();

        BacteriaSpawn.Instance.StartSpawn();
        UIManager.Instance.ActiveteLevelTextUI(LanguageManager.Instance.levelString + currentLevel.value + LanguageManager.Instance.getReadyString, false, false);
        UIManager.Instance.levelsUIText.gameObject.SetActive(false);
        UIManager.Instance.gameOverUIGO.SetActive(false);
        ReturnAllObjects();

        if(!DifficultManager.Instance.IsDieHardMode())
            ObjectSpawnManager.Instance.ActiveStartGameBoosters();
    }
    public void StartNewLevel()
    {

        CancelInvoke();
        MicrobManager.Instance.SetConditionMicrobSpawnTimer(true);

        currentLevel.value++;
        TextureManager.Instance.SetNewBg();
        BacteriaSpawn.Instance.StartNewLevel();
        StatisticsManager.Instance.SetTopLevel(currentLevel.value);

        Invoke("UIBetwenLevels", 1);
    }
    public void UIBetwenLevels()
    {
        levelResultSound.clip = VfxManager.Instance.GetSound("levelComplete");
        levelResultSound.Play();
        UIManager.Instance.ActiveteLevelTextUI(LanguageManager.Instance.levelString + currentLevel.value + LanguageManager.Instance.getReadyString, false, true);
    }
    public void StartNewCircleLevel()
    {
        int nextCircleSpawn = BacteriaSpawn.Instance.GetDefaultSpawn() + BossManager.Instance.GetHowMuchBossKilled();
        BacteriaSpawn.Instance.SetStartLevel(nextCircleSpawn);
        StartNewLevel();
    }

    private void LastBossKilledMenuOpen()
    {
        gameStarted = false;
        MenuManager.Instance.SetConditionBetwenLevel(false);
        MenuManager.Instance.SetConditionFirstStart(false);
        MenuManager.Instance.PauseGame();
        MenuManager.Instance.OpenRatingPanel(false);
        SurvivalModManager.Instance.survivalModPromoteUI.SetActive(true);
        VfxManager.Instance.PlayExplosionSound(3);
    }
    public void GameOver()
    {
        levelResultSound.clip = VfxManager.Instance.GetSound("levelDefete");
        levelResultSound.Play();

        gameStarted = false;
        BossManager.Instance.GetBossObject().SetActive(false);
        MicrobManager.Instance.GetMicrobGO().SetActive(false);

        UIManager.Instance.gameOverUIGO.SetActive(true);
        UIManager.Instance.gameOverUIText.text = LanguageManager.Instance.gameOverString;
        FindObjectOfType<MenuManager>().SetConditionFirstStart(false);

        Invoke("InvokeGameMenu", gameOverTextTimeOut);
    }
    public void CheckBacteriaInScene()
    {
        int activeBakterias = 0;
        for (int i = 0; i < ObjectPooling.Instance.GetParentArrayLength(); i++)
        {
            foreach (Transform child in ObjectPooling.Instance.GetParentOfBakteria(i).transform)
            {
                if (child.gameObject.activeSelf)
                    activeBakterias++;
            }
        }
        if (activeBakterias == 0)
        {
            if (!BossManager.Instance.GetBossObject().activeSelf)
            {
                if (!MicrobManager.Instance.GetMicrobGO().activeSelf)
                {
                    if (PlayerManager.Instance.GetLivesLeft() > 0)
                    {
                        if(SurvivalModManager.Instance.IsSurvivalModActive())
                        {
                            StartNewLevel();
                        }
                        else
                        {
                            if (BossManager.Instance.GetHowMuchBossKilled() >= 5)
                            {
                                MicrobManager.Instance.SetConditionMicrobSpawnTimer(false);
                                Invoke("LastBossKilledMenuOpen", 3);
                            }
                            else
                            {
                                BossManager.Instance.CheckBossNeed();
                            }
                        }
                    }
                }
            }
        }
    }
    public void AddPoints(int reward)
    {
        points.value += reward;
        StatisticsManager.Instance.SetPoints(points.value);
    }
    private void ReturnAllObjects()
    {
        ObjectPooling.Instance.ReturnObjectToPools();
        PlayerObject.Instance.SetBulletsPerShotCondition(false);
        PlayerManager.Instance.AddDoubleBulletTimer(0, true);
    }
}