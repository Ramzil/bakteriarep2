using UnityEngine;

public class MicrobManager : GenericSingletonClass<MicrobManager>
{
    [SerializeField]
    private GameObject microbGO;
    [SerializeField]
    private int microbSpeed;
    [SerializeField]
    private int microbRespawnTimeOutMin;
    [SerializeField]
    private int microbRespawnTimeOutMax;
    [SerializeField]
    private int rewardForMicrobKill = 500;

    [SerializeField]
    private bool microbCanSpawn = false;
    [SerializeField]
    private bool rewardBoosterForKill = false;
    [HideInInspector]
    private bool microbSpawnTimerActive = false;
    private float microbSpawnTime = 0f;

    [SerializeField]
    private float randomFloat = 5;

    private AudioSource microbVoice;

    public void SetConditionMicrobSpawnTimer(bool newCond)
    {
        microbSpawnTimerActive = newCond;
    }
    public int GetIntRewardForMicrobKill()
    {
        return rewardForMicrobKill;
    }
    public GameObject GetMicrobGO()
    {
        return microbGO;
    }
    void Start()
    {
        microbVoice = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (microbGO.activeSelf)
        {
            randomFloat -= Time.deltaTime;

            if (randomFloat <= 0)
                PlayMicrobRediculeVoice();
        }
        if(microbSpawnTimerActive)
        {
            microbSpawnTime -= Time.deltaTime;

            if (microbSpawnTime <= 0)
                MicrobRespawn();
        }
    }

    public void NewGame()
    {
        microbGO.SetActive(false);
        if (microbCanSpawn)
        {
            microbSpawnTime = Random.Range(microbRespawnTimeOutMin, microbRespawnTimeOutMax);
            microbSpawnTimerActive = true;
        }
    }

    public void MicrobRespawn()
    {
        microbSpawnTimerActive = false;
        float wichSide = Random.Range(0, 2) > 0 ? 11.25f : -11.25f;
        Vector2 spawnPos = new Vector2(wichSide, Random.Range(-5.5f, 5.5f));
        microbGO.transform.rotation = new Quaternion();
        microbGO.transform.position = spawnPos;
        microbGO.SetActive(true);
        float torque = Random.Range(-BacteriaSpawn.Instance.GetTorque(), BacteriaSpawn.Instance.GetTorque());
        Vector2 thrust = new Vector2(-wichSide, Random.Range(-2f, 2f));
        microbGO.GetComponent<Rigidbody2D>().AddForce(thrust * microbSpeed);
    }
    public void MicrobHide()
    {
        CheckEnemy();
    }

    public void MicrobDead()
    {
        microbVoice.clip = VfxManager.Instance.GetSound("microbDead");
        microbVoice.Play();
        VfxManager.Instance.PlayExplosionEffect(microbGO.transform.position);

        if (rewardBoosterForKill)
        {
            ObjectSpawnManager.Instance.BoosterSpecificSpawn(1, microbGO.transform.position, false, 1);
        }
        CheckEnemy();
    }

    private void CheckEnemy()
    {
        microbGO.SetActive(false);
        GameManager.Instance.CheckBacteriaInScene();
        microbSpawnTime = Random.Range(microbRespawnTimeOutMin, microbRespawnTimeOutMax);
        microbSpawnTimerActive = true;
    }

    public void PlayMicrobRediculeVoice()
    {
        microbVoice.clip = VfxManager.Instance.GetSound("microbRidicule");
        microbVoice.Play();
        randomFloat = Random.Range(3, 10);
    }
}