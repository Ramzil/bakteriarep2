using UnityEngine;

public class BulletPrefab : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        string collisionTag = collision.gameObject.tag;
        if (collisionTag == "Bacteria" || collisionTag == "MicrobBullet" || collision.gameObject.tag == "VirusObject")
        {
            ReturnToPool();
        }
        else if (collisionTag == "SpawnZone")
            Invoke(nameof(ReturnToPool), PlayerObject.Instance.GetTimeToHideBullet());
    }
    public void ReturnToPool()
    {
        ObjectPooling.Instance.ReturnBulletToPool(this);
    }
}
