using UnityEngine;

public class PlayerManager : GenericSingletonClass<PlayerManager>
{
    public GameObject player;
    private bool flickering = false;
    [SerializeField]
    private float flickeringDelay;
    private float flickeringTimer;
    [SerializeField]
    private int respawnTimeOut;
    [SerializeField]
    private int playerImmortality;
    private int livesLeft;
    [SerializeField]
    private int startPlayerLives;
    private bool doubleBulletTrue = false;
    [SerializeField]
    private float doubleBulletTimer = 0;
    private bool teleportActive = false;
    private int howMuchTeleport = 0;
    public BoosterScriptOneTime currentTeleport;
    [SerializeField]
    private Animation playerAnimation;

    private void Start()
    {
        livesLeft = startPlayerLives;
        player.SetActive(false);
    }

    public void AddDoubleBulletTimer(float addTime,bool isItReset)
    {
        if (isItReset) doubleBulletTimer = 0;
        else doubleBulletTimer += addTime;
    }
    public float GetDoubleBulletTimer()
    {
        return doubleBulletTimer;
    }
    public void SetStartLive(int newStartLive)
    {
        startPlayerLives = newStartLive;
    }
    public bool GetTeleportCondition()
    {
        return teleportActive;
    }
    public void SetLivesLeft(int newLiveNumber)
    {
        livesLeft += newLiveNumber;
    }
    public int GetLivesLeft()
    {
        return livesLeft;
    }

    private void Update()
    {
        if (flickering)
        {
            flickeringTimer += Time.deltaTime;

            if (flickeringTimer >= flickeringDelay)
            {
                flickeringTimer = 0;
                if (player.GetComponent<SpriteRenderer>().enabled)
                    player.GetComponent<SpriteRenderer>().enabled = false;
                else
                    player.GetComponent<SpriteRenderer>().enabled = true;
            }
        }

        if (doubleBulletTrue)
        {
            doubleBulletTimer -= Time.deltaTime;
            if (doubleBulletTimer <= 0)
            {
                PlayerObject.Instance.SetBulletsPerShotCondition(false);
                doubleBulletTrue = false;
            }
        }
    }

    public void PlayShotAnimation()
    {
        playerAnimation.Play();
    }

    public void SetDoubleBullet(bool condition)
    {
        doubleBulletTrue = condition;
    }

    public void TeleportActive(int numberTeleport)
    {
        teleportActive = true;
        howMuchTeleport += numberTeleport;
    }
    public void UpdateTeleport()
    {
        howMuchTeleport--;
        currentTeleport.howMuch--;
        currentTeleport.StartBoosterUI();
        if (howMuchTeleport <= 0)
        {
            currentTeleport.gameObject.SetActive(false);
            teleportActive = false;
        }
    }

    public void NewGame()
    {
        livesLeft = startPlayerLives;
        UIManager.Instance.UpdateLivesUi(startPlayerLives);
        teleportActive = false;
        doubleBulletTrue = false;
        howMuchTeleport = 0;
        PlayerRespawn();
    }

    public void PlayerDead()
    {
        VfxManager.Instance.PlayExplosionEffect(player.transform.position);
        VfxManager.Instance.PlayExplosionSound(2);
        livesLeft--;
        UIManager.Instance.UpdateLivesUi(livesLeft);
        player.SetActive(false);

        if (livesLeft < 1)
            GameManager.Instance.GameOver();
        else
            Invoke(nameof(PlayerRespawn), respawnTimeOut);
    }
    public void PlayerRespawn()
    {
        player.transform.position = Vector3.zero;
        player.transform.rotation = new Quaternion();
        player.layer = LayerMask.NameToLayer("IgnorCollisions");
        player.SetActive(true);
        flickering = true;
        Invoke(nameof(TurnOnCollisions), playerImmortality);
    }
    private void TurnOnCollisions()
    {
        flickering = false;
        player.GetComponent<SpriteRenderer>().enabled = true;
        player.layer = LayerMask.NameToLayer("Player");
    }

}
