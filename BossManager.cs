using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossManager : GenericSingletonClass<BossManager>
{
    [SerializeField]
    private SpriteRenderer bossRenderer;
    [SerializeField]
    private Color[] bossColors;

    [SerializeField]
    private int levelBetwenBossBattle = 10;

    [SerializeField]
    private int bossHealth = 10;
    [SerializeField]
    private int rewardForBossKill = 500;
    private int levelsLeft;

    [SerializeField]
    private bool bossCanSpawn = false;
    [SerializeField]
    private GameObject BossObject;
    [SerializeField]
    private GameObject BossHpUI;
    [SerializeField]
    private int bossSpeed;
    private bool boosKilled = false;
    [SerializeField]
    private int howMuchBossKilled;
    [SerializeField]
    private bool rewardBoosterForKill = false;
    [SerializeField]
    private float randomFloat = 5;
    [SerializeField]
    private Animation bossHeadAnim;
    [SerializeField]
    private Animator bossBodyAnim;
    private AudioSource bossVoice;

    public GameObject GetBossObject()
    {
        return BossObject;
    }

    public int GetBossHealth()
    {
        return bossHealth;
    }
    public int GetHowMuchBossKilled()
    {
        return howMuchBossKilled;
    }
    public int GetRewardBossKill()
    {
        return rewardForBossKill;
    }

    void Start()
    {
        bossVoice = GetComponent<AudioSource>();
        BossHpUI.SetActive(false);
        BossHpUI.GetComponent<Text>().font = LanguageManager.Instance.firstFont;
    }

    private void Update()
    {
        if (BossObject.activeSelf)
        {
            randomFloat -= Time.deltaTime;
            if (randomFloat <= 0)
                PlayBossRediculeVoice();
        }
    }

    public void NewGame()
    {
        CancelInvoke();
        StopAllCoroutines();
        levelsLeft = levelBetwenBossBattle;
        howMuchBossKilled = 0;
        BossObject.SetActive(false);
        BossHpUI.SetActive(false);
        bossRenderer.color = bossColors[howMuchBossKilled];
    }

    public void CheckBossNeed()
    {
        MicrobManager.Instance.SetConditionMicrobSpawnTimer(false);
        if (levelsLeft == 0 && bossCanSpawn) // start boss level
        {
            Invoke("StartSpawnBoss", 2);
        }
        else
        {
            levelsLeft--;
            Invoke("StartNewLevelOrCircle", 2);
        }
    }
    public void StartSpawnBoss()
    {
        GameManager.Instance.currentLevel.value++;
        StatisticsManager.Instance.SetTopLevel(GameManager.Instance.currentLevel.value);
        levelsLeft = levelBetwenBossBattle;
        UIManager.Instance.ActiveteLevelTextUI(LanguageManager.Instance.bossCommingString, true, false);
        VfxManager.Instance.PlayMusic(2);
        MicrobManager.Instance.SetConditionMicrobSpawnTimer(true);
    }

    public void StartNewLevelOrCircle()
    {
        if (boosKilled)
        {
            boosKilled = false;
            GameManager.Instance.StartNewCircleLevel();
        }
        else
        {
            GameManager.Instance.StartNewLevel();
        }
    }

    public void BossSpawn()
    {
        bossRenderer.color = bossColors[howMuchBossKilled];
        BossHpUI.SetActive(true);
        float wichSide = Random.Range(0, 2) > 0 ? 11.25f : -11.25f;
        Vector2 spawnPos = new Vector2(wichSide, Random.Range(-5.5f, 5.5f));
        BossObject.transform.rotation = new Quaternion();
        BossObject.transform.position = spawnPos;
        BossObject.SetActive(true);
        float torque = Random.Range(-BacteriaSpawn.Instance.GetTorque(), BacteriaSpawn.Instance.GetTorque());
        Vector2 thrust = new Vector2(-wichSide, Random.Range(-2f, 2f));
        BossObject.GetComponent<Rigidbody2D>().AddForce(thrust * bossSpeed);
        BossObject.GetComponent<Rigidbody2D>().AddTorque(torque);
    }

    public void UpdateBossHPUI(int bossHP)
    {
        BossHpUI.GetComponent<Text>().text = LanguageManager.Instance.GetBossHPWord() + bossHP;
    }

    public void BossDead()
    {
        bossBodyAnim.Play("boss_dead");
    }

    public void StartAnimationBossDead()
    {
        BossHpUI.SetActive(false);
        bossVoice.clip = VfxManager.Instance.GetSound("bossDead");
        bossVoice.Play();

        VfxManager.Instance.PlayMusic(0);

        howMuchBossKilled++;
        boosKilled = true;

        VfxManager.Instance.PlayExplosionEffect(BossObject.transform.position);
        VfxManager.Instance.PlayExplosionSound(1);

        BossObject.SetActive(false);

        UIManager.Instance.UpdateBossKilledUi();
        GameManager.Instance.CheckBacteriaInScene();

        if (rewardBoosterForKill)
        {
            int random = Random.Range(1, ObjectSpawnManager.Instance.achivment.Length);
            ObjectSpawnManager.Instance.BoosterSpecificSpawn(random, BossObject.transform.position, true,2);
        }
    }

    private void PlayBossRediculeVoice()
    {
        bossVoice.clip = VfxManager.Instance.GetSound("bossRidicule");
        bossVoice.Play();
        randomFloat = Random.Range(3, 10);
    }
}
