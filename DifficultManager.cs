using UnityEngine;
using UnityEngine.UI;

public class DifficultManager : GenericSingletonClass<DifficultManager>
{
    [SerializeField]
    private GameObject dieHardButton;
    [SerializeField]
    private GameObject pussyModeButton;

    // hard mode
    private bool dieHardMode = false;

    public int dieHardStartLive = 5;
    public float dieHardBoostSpawnLowInterval = 15;
    public float dieHardBoostSpawnHeightInterval = 30;
    public int dieHardMultiplyBoostForBossKill = 15;

    public int pussyModeLive = 10;
    public float pussyModeBoostSpawnLowInterval = 10;
    public float pussyModeBoostSpawnHeightInterval = 15;
    public int pussyModeMultiplyBoostForBossKill = 10;

    public bool IsDieHardMode()
    {
        return dieHardMode;
    }
    public int GetBoosReward()
    {
        if (dieHardMode)
        {
            return dieHardMultiplyBoostForBossKill;
        }
        else
        {
            return pussyModeMultiplyBoostForBossKill;
        }
    }

    public void SetHardMode(bool hardMode)
    {
        VfxManager.Instance.PlayClickButton();
        SurvivalModManager.Instance.survivalModPromoteUI.SetActive(false);

        dieHardMode = hardMode;
        if (dieHardMode)
        {
            dieHardButton.GetComponent<Image>().color = Color.green;
            pussyModeButton.GetComponent<Image>().color = Color.white;
            EventsManager.Instance.SetMaxBoosterSpawnTimer(dieHardBoostSpawnHeightInterval);
            EventsManager.Instance.SetMinBoosterSpawnTimer(dieHardBoostSpawnLowInterval);
            PlayerManager.Instance.SetStartLive(dieHardStartLive);
        }
        else if (!dieHardMode)
        {
            pussyModeButton.GetComponent<Image>().color = Color.green;
            dieHardButton.GetComponent<Image>().color = Color.white;
            EventsManager.Instance.SetMaxBoosterSpawnTimer(pussyModeBoostSpawnHeightInterval);
            EventsManager.Instance.SetMinBoosterSpawnTimer(pussyModeBoostSpawnLowInterval);
            PlayerManager.Instance.SetStartLive(pussyModeLive);
        }
        else
        {
            Debug.LogError("SetHardMode(): SOME ERROR");
        }
    }
}
