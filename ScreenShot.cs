using UnityEngine;

public class ScreenShot: MonoBehaviour
{
    public string ScreenName;
    public int numberScreens;

    public void TakeScreenShot()
    {
        numberScreens++;
        ScreenCapture.CaptureScreenshot($"{ScreenName}{numberScreens}.png");
    }
}