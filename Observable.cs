using System;

public class Observable<Tyty>
{
    public Observable(Tyty initValue)
    {
        _value = initValue;
    }

    Tyty _value;

    public event Action valueChanged;

    public Tyty value 
    {
        get { return _value; }
        set
        {
            if (!Equals(value, _value))
            {
                _value = value;

                if (valueChanged != null)
                    valueChanged.Invoke();
            }
        }
    }
}